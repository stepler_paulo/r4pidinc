<?php
class Api_model extends CI_Model {

	function __construct(){
		$this->load->database();
	}
	
	//organization api
	function get_organization_structure(){
		//query root node
		$this->db->select("employeeNumber, jobTitle, CONCAT(firstName,' ', lastName) AS name", FALSE);
		$query =  $this->db->get_where('employees', array('reportsTo' => NULL));
		$data = $query->result_array()[0];
		//async
		$data = $this->get_under($data['employeeNumber'],$data);
		echo json_encode($data);
		exit;
	}

	function get_under($employeeNumber,$data){
		
		$this->db->select("employeeNumber,reportsTo, jobTitle, CONCAT(firstName,' ', lastName) AS name", FALSE);
		$query =  $this->db->get_where('employees', array('reportsTo' => $employeeNumber));

		foreach($query->result_array() as $k => $d){
			$dt = $this->get_under($d['employeeNumber'],$d);
			$data['employeeUnder'][$k]=$dt;
		}
		return $data;
	}
	//end
	//offices api
	function get_offices_employee(){
		
		$this->db->select('offices.officeCode, city, employees.*');
		$this->db->from('offices');
		$this->db->join('employees', 'employees.officeCode = offices.officeCode');
		$this->db->order_by('offices.officeCode');
		$query = $this->db->get();
		
		$data = array();
		$oldOffice = '';
		$empIndex=0;
		$officeIndex=0;
		
		foreach($query->result_array() as $k => $d){
			if($oldOffice != $d['officeCode']) {
				$oldOffice = $d['officeCode'];
				$empIndex=0;
				if($k>0) $officeIndex++;
			}
			$data[$officeIndex]['officeCode']=$d['officeCode'];
			$data[$officeIndex]['city']=$d['city'];
			$data[$officeIndex]['employees'][$empIndex]['employeeNumber']= $d['employeeNumber'];
			$data[$officeIndex]['employees'][$empIndex]['name']= $d['firstName'].' '.$d['lastName'];
			$data[$officeIndex]['employees'][$empIndex]['jobTitle']= $d['jobTitle'];
			$empIndex++;
		}
		return $data;
	}
	//end
	//sales report api
	function get_sales_report($employeeNumber = FALSE){
		$this->db->select("
			employeeNumber,CONCAT(firstName,' ', lastName) AS name,
			jobTitle,offices.officeCode,offices.city,
			customerNumber,customerName", FALSE);
		$this->db->from('employees');
		$this->db->join('offices', 'offices.officeCode = employees.officeCode');
		$this->db->join('customers', 'customers.salesRepEmployeeNumber = employees.employeeNumber');	
		if($employeeNumber){
			$this->db->where('salesRepEmployeeNumber',$employeeNumber);
		}
		$query = $this->db->get();
	
	
		$data  = array();
		foreach($query->result_array() as $k => $d){
			$data[$k]['employeeNumber']=$d['employeeNumber'];
			$data[$k]['name']=$d['name'];
			$data[$k]['jobTitle']=$d['jobTitle'];
			$data[$k]['officeCode']=$d['officeCode'];
			$data[$k]['city']=$d['city'];
			$data[$k]['customerNumber']=$d['customerNumber'];
			$data[$k]['customerName']=$d['customerName'];
			$data[$k]['productLines']=$this->get_product_lines($d['customerNumber']);
			//break;
		}
		return $data;
	}
	
	function get_product_lines($customerNumber = FALSE){
		$query = $this->db->query("SELECT 
			`orders`.`customerNumber`,
			`customers`.`customerName`,
			`orderdetails`.`priceEach`,
			(`orderdetails`.`priceEach` * SUM(quantityOrdered)) as sales,
			`productlines`.`productLine`,
			`productlines`.`textDescription`,
			`products`.`productCode`,
			`products`.`productName`,
			`products`.`productScale`,
			SUM(quantityOrdered) AS quantity
			FROM
			  `orders` 
			  INNER JOIN `customers`
			  ON(`customers`.`customerNumber` = `orders`.`customerNumber`) 
			  INNER JOIN `orderdetails` 
				ON ( `orderdetails`.`orderNumber` = `orders`.`orderNumber`) 
			  INNER JOIN `products` 
				ON (`orderdetails`.`productCode` = `products`.`productCode`)   
			  INNER JOIN `productlines` 
				ON ( `products`.`productLine` = `productlines`.`productLine`) 
			WHERE `customers`.`customerNumber` = '$customerNumber'
			GROUP BY `products`.`productCode` 
		");
		$result = $query->result_array();
		
		$data = array();
		$productLine = array();
		$currentProductLine = '';
		$i=0;
		$pi=0;
		$totalCommision=0;
		foreach($result as $k=>$d){
			if($currentProductLine != $d['productLine'] && $k!=0){
				$currentProductLine = $d['productLine'];
				$productLine['commision']= number_format((float)$totalCommision, 2, '.', '');
				$data[$i] = $productLine;
				$productLine = array();
				$pi=0;$totalCommision=0;
				$i++;
			}
			
			$productLine['productLine']=$d['productLine'];
			$productLine['textDescription']=$d['textDescription'];
			$productLine['products'][$pi]['productCode']=$d['productCode'];
			$productLine['products'][$pi]['productName']=$d['productName'];
			$productLine['products'][$pi]['productScale']=$d['productScale'];
			$productLine['products'][$pi]['quantity']=$d['quantity'];
			$productLine['products'][$pi]['sales']=$d['sales'];
			
			//compute commission
			$explodedProductScale = explode(":", $d['productScale']);
			$percentage = $explodedProductScale[0]/$explodedProductScale[1];
			$commision = ($d['quantity']*$d['priceEach'])*$percentage;
			$productLine['products'][$pi]['commission'] =  number_format((float)$commision, 2, '.', '');
			//accumulate total commision
			$totalCommision+=$commision;
			
			//$productLine['products'][$pi]['priceEach'] = $d['priceEach'];
			//$productLine['products'][$pi]['percentage'] = $percentage;
			$pi++;
		}
		return $data;
	}
	//end
	
	//get sales report method 2
	function sales_report_method2($employeeNumber = FALSE){
		if($employeeNumber)
		{
			$query = $this->db->query("
				SELECT 
				  employeeNumber,
				  CONCAT(firstName, ' ', lastName) AS `name`,
				  jobTitle,
				  offices.officeCode,
				  offices.city,
				  GROUP_CONCAT(
					DISTINCT customerNumber 
					ORDER BY customerNumber SEPARATOR ', '
				  ) AS employeeCustomers 
				FROM
				  `customers` 
				  INNER JOIN `employees` 
					ON ( `customers`.`salesRepEmployeeNumber` = `employees`.`employeeNumber`) 
				  INNER JOIN `offices` 
					ON (  `employees`.`officeCode` = `offices`.`officeCode`) 
				WHERE salesRepEmployeeNumber = '$employeeNumber' 
				GROUP BY employeeNumber ;");
		}
		else
		{
			$query = $this->db->query("
				SELECT 
				  employeeNumber,
				  CONCAT(firstName, ' ', lastName) AS `name`,
				  jobTitle,
				  offices.officeCode,
				  offices.city,
				  GROUP_CONCAT(
					DISTINCT customerNumber 
					ORDER BY customerNumber SEPARATOR ', '
				  ) AS employeeCustomers 
				FROM
				  `customers` 
				  INNER JOIN `employees` 
					ON ( `customers`.`salesRepEmployeeNumber` = `employees`.`employeeNumber`) 
				  INNER JOIN `offices` 
					ON (  `employees`.`officeCode` = `offices`.`officeCode`)
				GROUP BY employeeNumber ;");
		}
		
		$data = $query->result_array();
		foreach($data as $k=>$d){
			$salesData = $this->sales_data($d['employeeCustomers']);
			$data[$k]['totalCommision'] = $salesData[2];
			$data[$k]['totalSales'] = $salesData[1];
			$data[$k]['productLines'] = $salesData[0];
			unset($data[$k]['employeeCustomers']);
		}
		echo json_encode($data);
		exit;
	}
	
	function sales_data($employeeCustomers = false){
		$query = $this->db->query(	
			"SELECT 
			  COUNT(*),
			  SUM(`orderdetails`.`quantityOrdered`) AS quantity,
			  `orderdetails`.`priceEach`,
			  (`orderdetails`.`priceEach` * SUM(`orderdetails`.`quantityOrdered` )) as sales,
			  `products`.`productCode`,
			  `products`.`productName`,
			  `products`.`productScale`,
			  `productlines`.`productLine`,
			  `productlines`.`textDescription`
			FROM
			  `orderdetails` 
			  INNER JOIN `orders` 
				ON ( `orderdetails`.`orderNumber` = `orders`.`orderNumber`) 
			  INNER JOIN `products` 
				ON ( `orderdetails`.`productCode` = `products`.`productCode`) 
			  INNER JOIN `productlines` 
				ON (  `products`.`productLine` = `productlines`.`productLine`) 
			WHERE `orders`.`customerNumber` IN ($employeeCustomers) 
			GROUP BY `products`.`productCode` 
			ORDER BY `productlines`.`productLine`"
		);
		$result = $query->result_array();
		
		$data = array();
		$productLine = array();
		$currentProductLine = '';
		$i=0;
		$pi=0;
		$totalQuantity=0;
		$productLineTotalCommision=0;
		$productLineTotalSales=0;
		$employeeTotalSales=0;
		$employeeTotalCommision=0;
		foreach($result as $k=>$d){
			if($currentProductLine != $d['productLine'] && $k!=0){
				$productLine['productLine']=$d['productLine'];
				$productLine['textDescription']=$d['textDescription'];
				$currentProductLine = $d['productLine'];
				$productLine['quantity']=$totalQuantity;
				$productLine['sales']=number_format((float)$productLineTotalSales, 2, '.', '');;
				$productLine['commision']= number_format((float)$productLineTotalCommision, 2, '.', '');
				$employeeTotalSales+=$productLine['sales'];
				$employeeTotalCommision+=$productLine['commision'];
				$data[$i] = $productLine;
				$productLine = array();
				$pi=0;
				$productLineTotalCommision=0;
				$totalQuantity=0;
				$productLineTotalSales=0;
				$i++;
			}
			$productLine['products'][$pi]['productCode']=$d['productCode'];
			$productLine['products'][$pi]['productName']=$d['productName'];
			//$productLine['products'][$pi]['productScale']=$d['productScale'];
			$productLine['products'][$pi]['quantity']=$d['quantity'];
			$productLine['products'][$pi]['sales']=$d['sales'];
			//compute commission
			$explodedProductScale = explode(":", $d['productScale']);
			$percentage = $explodedProductScale[0]/$explodedProductScale[1];
			$commision = ($d['quantity']*$d['priceEach'])*$percentage;
			//accumulate product line total commision/sales
			$totalQuantity+=$d['quantity'];
			$productLineTotalCommision+=$commision;
			$productLineTotalSales+=$d['sales'];
			//increment product index 
			$pi++;
		}
	
		$total = array();
		return array($data,$employeeTotalSales,$employeeTotalCommision);
	}
}
