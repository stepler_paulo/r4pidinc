<?php
class Api extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('api_model');
		$this->load->helper('url_helper');
	}

	function organization(){
		$data = $this->api_model->get_organization_structure();
		echo json_encode($data);
		exit;
	}
	
	function offices(){
		$data = $this->api_model->get_offices_employee();
		echo json_encode($data);
		exit;
	}
	
	function sales_report($employeeNumber=false){
		$data = $this->api_model->get_sales_report($employeeNumber);
		echo json_encode($data);
		exit;
	}
}